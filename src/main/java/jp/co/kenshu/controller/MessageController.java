package jp.co.kenshu.controller;

import java.util.List;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.test.CommentDto;
import jp.co.kenshu.dto.test.MessageDto;
import jp.co.kenshu.form.CommentForm;
import jp.co.kenshu.form.MessageForm;
import jp.co.kenshu.service.MessageService;

@Controller
public class MessageController {

    @Autowired
    private MessageService messageService;

    @RequestMapping(value = "/home/", method = RequestMethod.GET)
    public String messageAll(Model model) {
        List<MessageDto> messages = messageService.getMessageAll();
        model.addAttribute("title", "匿名掲示板「20チャンネル」");
        model.addAttribute("messages", messages);
        return "home";
    }

    @RequestMapping(value = "/message/{id}", method = RequestMethod.POST)
    public String commentInsert(@ModelAttribute CommentForm form, Model model) {
    	int count = messageService.insertComment(form.getText(), form.getMessageId());
        Logger.getLogger(MessageController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
        return "redirect:/message/{id}";
    }

    @RequestMapping(value = "/message/{id}", method = RequestMethod.GET)
    public String messageDto(Model model, @PathVariable int id) {
        MessageDto dto = new MessageDto();
        dto.setId(id);
        MessageDto message = messageService.getMessageByDto(dto);
        List<CommentDto> comments = messageService.getCommentAll();
        CommentForm form = new CommentForm();
        model.addAttribute("CommentForm", form);
        model.addAttribute("title", "投稿内容");
        model.addAttribute("message", message);
        model.addAttribute("comments", comments);
        return "message";
    }

    @RequestMapping(value = "/create/message/", method = RequestMethod.GET)
    public String messageInsert(Model model) {
        MessageForm form = new MessageForm();
        model.addAttribute("MessageForm", form);
        model.addAttribute("title", "新規投稿");
        return "createMessage";
    }

    @RequestMapping(value = "/create/message/", method = RequestMethod.POST)
    public String messageInsert(@ModelAttribute MessageForm form, Model model) {
        int count = messageService.insertMessage(form.getText());
        Logger.getLogger(MessageController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
        return "redirect:/home/";
    }
}
