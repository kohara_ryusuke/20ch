package jp.co.kenshu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.kenshu.dto.test.MessageDto;
import jp.co.kenshu.entity.Comment;
import jp.co.kenshu.entity.Message;

public interface Mapper {
    List<Message> getMessageAll();
    Message getMessageByDto(MessageDto dto);
    int insertMessage(String text);
    List<Comment> getCommentAll();
    int insertComment(@Param("text")String text, @Param("messageId")int messageId);
}
