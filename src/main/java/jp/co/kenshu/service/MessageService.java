package jp.co.kenshu.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kenshu.dto.test.CommentDto;
import jp.co.kenshu.dto.test.MessageDto;
import jp.co.kenshu.entity.Comment;
import jp.co.kenshu.entity.Message;
import jp.co.kenshu.mapper.Mapper;

@Service
public class MessageService {

    @Autowired
    private Mapper mapper;

    public List<MessageDto> getMessageAll() {
        List<Message> testList = mapper.getMessageAll();
        List<MessageDto> resultList = convertToDto(testList);
        return resultList;
    }

    private List<MessageDto> convertToDto(List<Message> testList) {
        List<MessageDto> resultList = new LinkedList<>();
        for (Message entity : testList) {
            MessageDto dto = new MessageDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }

    public MessageDto getMessageByDto(MessageDto dto) {
        Message entity = mapper.getMessageByDto(dto);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public int insertMessage(String text) {
        int count = mapper.insertMessage(text);
        return count;
    }

    public List<CommentDto> getCommentAll() {
        List<Comment> testList = mapper.getCommentAll();
        List<CommentDto> resultList = convertToDtoC(testList);
        return resultList;
    }

    private List<CommentDto> convertToDtoC(List<Comment> testList) {
        List<CommentDto> resultList = new LinkedList<>();
        for (Comment entity : testList) {
            CommentDto dto = new CommentDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }

    public int insertComment(String text, int messageId) {
        int count = mapper.insertComment(text, messageId);
        return count;
    }


}
