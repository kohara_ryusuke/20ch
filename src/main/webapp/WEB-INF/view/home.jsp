<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>ホーム</title>
        <link href="/resources/css/home.css" rel="stylesheet" type="text/css">
    </head>
    <body>
    	<a href="http://localhost:8080/20ch/create/message/" class="url">新規投稿</a>

		<h1>${title}</h1>
        <ol>
        <c:forEach items="${messages}" var="message">
        	<li class="text"><a href="http://localhost:8080/20ch/message/${message.id}/">${message.text}</a>
        	<br>
        </c:forEach>
        </ol>
    </body>
</html>