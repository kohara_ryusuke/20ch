<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8">
<title>新規投稿</title>
</head>
<body>
	<a href="http://localhost:8080/20ch/home/" class="url">投稿一覧</a>

    <h1>${title}</h1>
    <form:form modelAttribute="MessageForm">
        <form:textarea path="text" cols="30" row="5"/>
        <input type="submit" value="投稿">
    </form:form>
</body>
</html>