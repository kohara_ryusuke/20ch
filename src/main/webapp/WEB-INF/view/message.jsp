<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
    <head>
        <meta charset="utf-8">
        <title>投稿スレ</title>
    </head>
    <body>
    	<a href="http://localhost:8080/20ch/home/" class="url">投稿一覧</a>

           <h1>${title}</h1>
        <h2>${message.text}</h2>

		<ol>
        <c:forEach items="${comments}" var="comment">
	        <c:if test="${comment.messageId == message.id}">
	        	<li class="text"><c:out value="${comment.text}"></c:out><br>
	        	<div class="date">
					<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
				</div>
	        	<br>
	        </c:if>
        </c:forEach>
        </ol>

        <form:form modelAttribute="CommentForm">
	        <form:textarea path="text" cols="30" row="5"/>
	        <input type="hidden" name="messageId" value="${message.id}">
	        <input type="submit" value="投稿">
	    </form:form>
    </body>
</html>